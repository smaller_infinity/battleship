# Changelog for battleship

## 18/12/30 - 0.3.1
- Now allows computer to start first.

## 18/12/29 - 0.3
- Added a smarter backend that searches for moves before acting.

## 18/12/28 - 0.2.1
- Multiple play throughs supported in single session
- Tracks scores
- Win screen

## 18/12/28 - 0.2.0
- Initial working version
- Single play through
- Dumb 'pure random' backend
