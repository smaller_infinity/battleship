module BattleShip.GameTest(testGame) where

import Test.Tasty(TestTree,testGroup)
import qualified Data.Vector as V
import Utils(runTests)
import BattleShip.Game(DefenseBoard,DefensePiece(..),validBoard,emptyDefenseBoard)

testGame :: TestTree
testGame = testGroup "Game State Tests"
  [ runTests validBoard "validBoardTests" validBoardTests
  ]

validBoardTests :: [(String,DefenseBoard,Bool)]
validBoardTests =
  [ ("Empty Board",emptyDefenseBoard,False)
  ]
