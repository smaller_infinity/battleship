import Test.Tasty(defaultMain,testGroup)

import BattleShip.GameTest(testGame)

main :: IO ()
main = defaultMain $ testGroup "Test" [testGame]
