module Utils
  ( runTests
  , runTests2
  , runTests3
  , runTests4
  ) where

import Test.Tasty(TestTree,testGroup)
import Test.Tasty.HUnit(testCase,(@?=))

runTests :: (Show b,Eq b) => (a -> b) -> String -> [(String,a,b)] -> TestTree
runTests f name = testGroup name .
  map (\(n,i,o) -> testCase n $ f i @?= o)

runTests2 :: (Show c,Eq c) => (a -> b -> c) -> String -> [(String,a,b,c)]
  -> TestTree
runTests2 f name = testGroup name .
  map (\(n,a,b,o) -> testCase n $ f a b @?= o)

runTests3 :: (Show d,Eq d) => (a -> b -> c -> d) -> String -> [(String,a,b,c,d)]
  -> TestTree
runTests3 f name = testGroup name .
  map (\(n,a,b,c,o) -> testCase n $ f a b c @?= o)

runTests4 :: (Show e,Eq e) => (a -> b -> c -> d -> e) -> String
  -> [(String,a,b,c,d,e)] -> TestTree
runTests4 f name = testGroup name .
  map (\(n,a,b,c,d,o) -> testCase n $ f a b c d @?= o)
