module BattleShip.Game
  ( GameState(..)
  , AttackBoard
  , AttackPiece(..)
  , DefenseBoard
  , DefensePiece(..)
  , Ship(..)
  , randomGameState
  , emptyDefenseBoard
  , validBoard
  , hasWon
  , attack
  , ships
  ) where

import Data.Vector(Vector)
import qualified Data.Vector as V
import Control.Monad(foldM)
import System.Random(randomRIO)

-- |The five ships possible.
data Ship = Carrier
          | BattleShip
          | Crusier
          | Submarine
          | Destroyer
  deriving(Eq,Ord)

-- |Each attack piece can either be empty, a hit, or a miss.
data AttackPiece = AEmpty | Hit Ship | Miss
  deriving(Eq,Ord)

-- |Each defense piece can either be empty, or one of the boats.
data DefensePiece = DEmpty | Occupied Ship
  deriving(Eq)

-- |An attack board is just a vector of 64 attack pieces.
type AttackBoard = Vector AttackPiece

-- |A defense board is just a vector of 64 defense pieces.
type DefenseBoard = Vector DefensePiece

-- |The current state is just two sets of boards.
-- |The P's denote who owns each board.
data GameState = GameState { attackP1 :: AttackBoard
                           , defenseP1 :: DefenseBoard
                           , attackP2 :: AttackBoard
                           , defenseP2 :: DefenseBoard
                           }

-- |Builds an empty DefenseBoard.
emptyDefenseBoard :: DefenseBoard
emptyDefenseBoard = V.replicate 64 DEmpty

-- |Builds an empty AttackBoard.
emptyAttackBoard :: AttackBoard
emptyAttackBoard = V.replicate 64 AEmpty

-- |Checks if an attack board has won.
hasWon :: AttackBoard -> Bool
hasWon = (==) 17 . V.length . V.filter isHit
  where
    isHit (Hit _) = True
    isHit _ = False

ships :: [(Ship,Int)]
ships = [(Carrier,5),(BattleShip,4),(Crusier,3),(Submarine,3),(Destroyer,2)]

-- |Checks if a defense board is valid (all pieces, all ships right length, all
-- ship pieces next to each other in a line).
validBoard :: DefenseBoard -> Bool
validBoard board = (&&) (17 == V.length ( V.filter isOccupied board)) $
  all (uncurry checkPType) ships
  where
    isOccupied (Occupied _) = True
    isOccupied _ = False
    checkPType :: Ship -> Int -> Bool
    checkPType s n = case V.findIndex (== Occupied s) board of
      Just i -> checkPiece i (Occupied s == board V.! (i + 1)) n
      _ -> False
    checkPiece :: Int -> Bool -> Int -> Bool
    checkPiece start isHorizontial n =
      let ns = take n $ if isHorizontial then [start..]
                        else [start,start + 8..]
          pType = board V.! start in
        all (\i -> pType == board V.! i) $ tail ns

-- |If possible, places a piece on a defense board.
placePiece :: DefenseBoard -> Ship -> Bool -> Int -> Int -> Maybe DefenseBoard
placePiece board ship isHorizontial start n
  |isHorizontial && 8 - mod start 8 >= n =
     let vals = take n [start..] in
       if any (/= DEmpty) $ map (\i -> board V.! i) vals then Nothing
       else Just $ (V.//) board $ map (\i -> (i,Occupied ship)) vals
  |not isHorizontial && 8 - div start 8 >= n =
     let vals = take n [start,start+8..] in
       if any (/= DEmpty) $ map (\i -> board V.! i) vals then Nothing
       else Just $ (V.//) board $ map (\i -> (i,Occupied ship)) vals
  |otherwise = Nothing

-- |Builds a random game state.
randomGameState :: IO GameState
randomGameState = placeRandom >>=
  \d1 -> placeRandom >>=
  \d2 -> return GameState { attackP1 = emptyAttackBoard
                          , attackP2 = emptyAttackBoard
                          , defenseP1 = d1
                          , defenseP2 = d2
                          }

-- |Builds a random defense board.
placeRandom :: IO DefenseBoard
placeRandom = foldM place emptyDefenseBoard ships
  where
    place board val@(p,n) =
      randomRIO (0,63) >>=
      \mv -> randomRIO (0 :: Int,1) >>=
      \isH -> case placePiece board p (isH == 0) mv n of
                Just x -> return x
                _ -> place board val

-- |Takes one players attack board and the others defense board and attempts to
-- 'attack' a piece ([0,63]).
-- If a legal move, returns the new attack board, otherwise returns nothing.
attack :: AttackBoard -> DefenseBoard -> Int -> Maybe AttackBoard
attack aBoard dBoard mv =
  case (aBoard V.!? mv, dBoard V.!? mv) of
    (Just AEmpty,Just DEmpty) -> Just $ aBoard V.// [(mv,Miss)]
    (Just AEmpty,Just (Occupied s)) -> Just $ aBoard V.//[(mv,Hit s)]
    _ -> Nothing
