{-# LANGUAGE RecordWildCards #-}
module BattleShip.Interface(runApp) where

import Data.List.Split(chunksOf)
import qualified Data.Vector as V
import Control.Monad(void)
import Control.Monad.IO.Class(liftIO)
import System.Random(randomRIO)

import Graphics.Vty(defAttr)
import Graphics.Vty.Input.Events(Event(..),Key(..))
import Brick.Main(App(..),continue,defaultMain,halt,showFirstCursor)
import Brick.Types(Widget,BrickEvent(..),Next,EventM,Location(..))
import Brick.Widgets.Core(vBox,hBox,fill,hLimit,vLimit,showCursor,padLeftRight,str)
import Brick.Widgets.Border(border)
import Brick.AttrMap(attrMap)

import BattleShip.Game(GameState(..),DefenseBoard,DefensePiece(..),Ship(..),
                       AttackBoard,AttackPiece(..),randomGameState,attack,hasWon)
import BattleShip.Game.Strats.Random(smartRandom,pureRandom)

-- |Tracks the state of the game, which is just the core gamestate,
-- plus the current player (assumes that player one always goes first),
-- which player the player on this machine is, and
-- the type of the game it is.
data AppState = AppState { gamestate :: GameState
                         , userNumber :: Int
                         , gametype :: GameType
                         , cursorLoc :: (Int,Int)
                         -- |A note about scores, the first score is always the player's
                         -- score, regardless of what number player they are.
                         , scores :: (Int,Int)
                         }
              | Winner { userNumber :: Int
                       , winner :: Int
                       , scores :: (Int,Int)
                       , gametype :: GameType
                       }

data Backend = PureRandom | SmartRandom

data GameType = PlayerVsComputer Backend

runApp :: IO ()
runApp = let app = App { appDraw = bAppDraw
                       , appStartEvent = bAppStartEvent
                       , appHandleEvent = bAppHandleEvent
                       , appChooseCursor = showFirstCursor
                       , appAttrMap = const $ attrMap Graphics.Vty.defAttr []
                       } in
  randomGameState >>=
  \initState -> generateUser >>=
  \user -> void $ defaultMain app AppState
           { gamestate = initState
           , userNumber = user
           , gametype = PlayerVsComputer SmartRandom
           , cursorLoc = (0,0)
           , scores = (0,0)
           }

generateUser :: IO Int
generateUser = randomRIO (1,2)

bAppDraw :: AppState -> [Widget Int]
bAppDraw Winner {..} = [str $ "Player " ++ show winner ++
                        " Won!\nPress n to continue."]
bAppDraw AppState{gamestate=GameState{..},..} =
  [vBox
    [ str top
    , hBox
      [ limit $ drawDefBoard defenseP1 attackP2
      , showCursor 0 (Location $ modCursor cursorLoc) $
        limit $ drawAttBoard attackP1
      ]]]
  where
    limit = border . vLimit 8 . hLimit 24
    modCursor (x,y) = ((3 * x)+2,y+1)
    top = "You Are Player " ++ show userNumber ++
      "\nYour Score: " ++ show (fst scores) ++
      "\nTotal Games: " ++ show (uncurry (+) scores)

-- |Takes an attack board and produces a single widget.
drawAttBoard :: AttackBoard -> Widget n
drawAttBoard = vBox . map (hBox . map (padLeftRight 1 . drawPiece)) .
  chunksOf 8 . V.toList
  where
    drawPiece AEmpty = fill ' '
    drawPiece Miss = fill 'o'
    drawPiece (Hit Carrier) = fill 'C'
    drawPiece (Hit BattleShip) = fill 'B'
    drawPiece (Hit Crusier) = fill 'c'
    drawPiece (Hit Submarine) = fill 'S'
    drawPiece (Hit Destroyer) = fill 'D'

-- |Takes an defense board and produces a single widget.
drawDefBoard :: DefenseBoard -> AttackBoard -> Widget n
drawDefBoard dBoard aBoard =
  vBox $ map (hBox . map (padLeftRight 1 . drawPiece)) $ chunksOf 8 $
  zip (V.toList dBoard) (V.toList aBoard)
  where
    drawPiece (_,Miss) = fill 'o'
    drawPiece (_,Hit _) = fill '*'
    drawPiece (DEmpty,AEmpty) = fill ' '
    drawPiece (Occupied Carrier,_) = fill 'C'
    drawPiece (Occupied BattleShip,_) = fill 'B'
    drawPiece (Occupied Crusier,_) = fill 'c'
    drawPiece (Occupied Submarine,_) = fill 'S'
    drawPiece (Occupied Destroyer,_) = fill 'D'

bAppHandleEvent :: AppState -> BrickEvent n e -> EventM n (Next AppState)
bAppHandleEvent s (VtyEvent (EvKey (KChar 'q') _)) = halt s
bAppHandleEvent Winner{..} (VtyEvent (EvKey (KChar 'n') _)) =
  liftIO randomGameState >>=
  \initState@GameState{..} -> liftIO generateUser >>=
  \user -> let state =  AppState { gamestate = initState
                                 , userNumber = user
                                 , scores = scores
                                 , gametype = gametype
                                 , cursorLoc = (0,0)
                                 } in
    (case gametype of
       PlayerVsComputer PureRandom ->
         liftIO (pureRandom attackP2 defenseP1)
       PlayerVsComputer SmartRandom ->
         liftIO (smartRandom attackP2 defenseP1)) >>=
    \attackP2' -> continue $ state{gamestate=initState{attackP2=attackP2'}}
bAppHandleEvent s@AppState{..} (VtyEvent (EvKey KLeft _))
  |fst cursorLoc > 0 = continue $ s{cursorLoc = (fst cursorLoc - 1, snd cursorLoc)}
  |otherwise = continue s
bAppHandleEvent s@AppState{..} (VtyEvent (EvKey KRight _))
  |fst cursorLoc < 7 = continue $ s{cursorLoc = (fst cursorLoc + 1, snd cursorLoc)}
  |otherwise = continue s
bAppHandleEvent s@AppState{..} (VtyEvent (EvKey KUp _))
  |snd cursorLoc > 0 = continue $ s{cursorLoc = (fst cursorLoc, snd cursorLoc - 1)}
  |otherwise = continue s
bAppHandleEvent s@AppState{..} (VtyEvent (EvKey KDown _))
  |snd cursorLoc < 7 = continue $ s{cursorLoc = (fst cursorLoc, snd cursorLoc + 1)}
  |otherwise = continue s
bAppHandleEvent s@AppState{gamestate=g@GameState{..},..}
  (VtyEvent (EvKey (KChar ' ') _)) =
  case attack attackP1 defenseP2 (calcIndex cursorLoc) of
    Just attackP1' -> let p1 = s{gamestate=g{attackP1=attackP1'}} in
      if hasWon attackP1' then continue Winner { winner = userNumber
                                               , userNumber = userNumber
                                               , scores = updateScore userNumber
                                               , gametype = gametype
                                               }
      else
        (case gametype of
           PlayerVsComputer PureRandom ->
             liftIO (pureRandom attackP2 defenseP1)
           PlayerVsComputer SmartRandom ->
             liftIO (smartRandom attackP2 defenseP1)) >>=
           \attackP2' -> let p2 = p1{gamestate=(gamestate p1){attackP2=attackP2'}} in
             if hasWon attackP2' then continue Winner { winner = 2
                                                      , userNumber = userNumber
                                                      , scores = updateScore 2
                                                      , gametype = gametype
                                                      }
             else continue p2
    _ -> continue s
  where
    calcIndex (x,y) = (y * 8) + x
    updateScore winner = if userNumber == winner then (fst scores + 1,snd scores)
                         else (fst scores,snd scores + 1)
bAppHandleEvent s _ = continue s

bAppStartEvent :: AppState -> EventM n AppState
bAppStartEvent s@AppState{gamestate=g@GameState{..},..} =
  if userNumber == 1 then return s
  else (case gametype of
    PlayerVsComputer PureRandom ->
             liftIO (pureRandom attackP2 defenseP1)
    PlayerVsComputer SmartRandom ->
             liftIO (smartRandom attackP2 defenseP1)) >>=
       \attackP2' -> return s{gamestate=g{attackP2=attackP2'}}
bAppStartEvent x = return x
