module BattleShip.Game.Strats.Random(pureRandom,smartRandom) where

import qualified Data.Map.Strict as M
import Data.List(sortBy,groupBy,nub)
import qualified Data.Vector as V
import System.Random(randomRIO)
import BattleShip.Game(AttackBoard,AttackPiece(..),DefenseBoard,attack,ships)

-- |Simplest possible random movement.  Very unsafe.  Bad idea.
pureRandom :: AttackBoard -> DefenseBoard -> IO AttackBoard
pureRandom aBoard dBoard =
  randomRIO (0,63) >>=
  \p -> case attack aBoard dBoard p of
          Just x -> return x
          _ -> pureRandom aBoard dBoard

-- |An improved random attack that looks for better moves.
smartRandom :: AttackBoard -> DefenseBoard -> IO AttackBoard
smartRandom aBoard dBoard =
  case foldr findMove Nothing $ extractMoves aBoard of
    Just x -> return x
    _ -> pureRandom aBoard dBoard
  where
    findMove val acc = case acc of
      Just x -> Just x
      _ -> attack aBoard dBoard val

-- |Pulls out the hits along side their indices then filters the sunk
-- ships.
extractMoves :: AttackBoard -> [Int]
extractMoves = let ships' = M.fromList ships in
  nub . concatMap (generateMoves . map fst) .
  filter (\vals -> length vals /= ships' M.! getPeice vals) .
  sortBy (\a b -> compare (length b) (length a)) .
  groupBy (\a b -> snd a == snd b) .
  sortBy (\a b -> compare (snd a) (snd b)) . map (\(x,Hit y) -> (x,y)) .
  filter isHit . zip [0..] . V.toList
  where
    isHit (_,Hit _) = True
    isHit _ = False
    getPeice ((_,p):_) = p
    generateMoves [x] = [x+1,x-1,x+8,x-8]
    generateMoves [x,y] = let dif = abs $ y - x in
      [x-dif,y + dif]
    generateMoves (x:y:xs) = let dif = abs $ y - x in
      [x-dif,last xs + dif]
